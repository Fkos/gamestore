@extends('layouts.app')

@push('style')
    .card-register{
        max-width : 40rem;
    }

    datalist {
        display: none;
    }
@endpush

@section('content')
<div class="container">
    <div class="card card-register mx-auto my-auto">
        <div class="card-header">{{ __('Register') }}</div>
        <div class="card-body">
            <form method="POST" action="{{ route('register') }}">
                @csrf
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <label for="name">{{ __('First Name') }}</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <label for="surname">{{ __('Last Name') }}</label>
                                <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required autocomplete="surname" autofocus>
                                @error('surname')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <label for="country">{{ __('Search your Country') }}</label>
                                <input list="countries" name="country" id="country" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ old('country') }}" required>
                                <datalist id="countries">
                                    @foreach($countries as $country)
                                    <option value="{{ $country->name }}">{{ $country->name.' '.$country->phone_code }}</option>
                                    @endforeach
                                </datalist>
                                @error('country')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <label for="phone">{{ __('Phone') }}</label>
                                <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus>
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">

                    <div class="form-label-group">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">

                    <div class="form-label-group">
                        <label for="address">{{ __('Shipping Address') }}</label>
                        <textarea id="address" class="form-control @error('address') is-invalid @enderror" name="address" required autocomplete="address">{{ old('address') }}</textarea>

                        @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <label for="password">{{ __('Password')}}</label>
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-label-group">
                                <label for="password-confirm">{{ __('Confirm Password')}}</label>
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="want_newsletter" id="want_newsletter" value="1" checked>
                        {{ __('Get notified of new products') }}
                      </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Register') }}
                </button>
            </form>
            <div class="text-center">
                <a class="d-block small mt-3" href="{{route('login')}}">{{ __('Login') }}</a>
                {{-- @if (Route::has('password.request'))
                <a class="d-block small" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                @endif --}}
            </div>
        </div>
    </div>
</div>
@endsection
