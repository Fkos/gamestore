@extends('layouts.app')

@push('style')    
.card-login {
    max-width: 25rem;
}
@endpush

@section('content')
<div class="container">
    <div class="card card-login mx-auto mt-5">
        <div class="card-header">{{ __('Login') }}</div>
        
        <div class="card-body">
            <form method="POST" action="{{ $route }}">
                @csrf
                
                <div class="form-group">
                    <div class="form-label-group">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input type="email" id="email" class="form-control  @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-label-group">
                        <label for="password">{{__('Password')}}</label>
                        <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"  name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            {{ __('Remember Me') }}
                        </label>
                    </div>
                </div>
                <button class="btn btn-primary btn-block" type="submit">{{ __('Login') }}</button>
                
            </form>
            @if($route != route('deep.login'))
            <div class="text-center">
                @if (Route::has('register'))
                <a class="d-block small mt-3" href="{{route('register')}}">{{ __('Register an Account') }}</a>
                @endif
                @if (Route::has('password.request'))
                <a class="d-block small" href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
                @endif
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
