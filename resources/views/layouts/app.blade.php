<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="S'authentifier sur votre compte Game Store!">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Game Store') }} @yield('title-complement')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        .navbar {
            background-color: tomato;
        }

        h1,h2,h3,h4,h5,h6 {
            color: tomato;
        }
        @stack('style')
    </style>
    @stack('head')
</head>
<body class="bg-white">
    <div id="app">

        @yield('menu', View::make('partials.menu'))
        <main class="pt-4">
            @yield('content')
        </main>
    </div>
    @stack('end')
</body>
</html>
