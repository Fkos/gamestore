@extends('layouts.app')

@push('style')
.btn-buy{
    background-color : tomato;
    color : black;
}
@endpush

@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-3 position-sticky">
            
            <h3 class="my-2">{{ __('Categories') }}</h3>
            <div class="list-group">
                @foreach($categories as $category)
                <a href="{{action('WelcomeController@productOfCategory', [$category->id, $category->name])}}" class="list-group-item text-dark"><i class="{{$category->fa_class}}"></i> {{ __($category->name) }}</a>
                @endforeach
            </div>
        </div>
        <div class="col-lg-9">
            <h3 class="my-2" style="color: tomato">{{ $product->name }}</h3>
            <div class="card mt-4">
                @if (count($product->images))
                    <div id="productImages" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            @for ($i = 0; $i < count($product->images); $i++)
                                <li data-target="#productImages" @if($i == 0)data-slide-to="0" class="active"@endif></li>
                            @endfor
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            @foreach ($product->images as $image)
                                <div class="carousel-item active">
                                    <img src="{{ asset($image->url) }}" alt="First slide">
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#productImages" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#productImages" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                @endif
                <div class="card-body">
                    <h3 class="card-title">{{ $product->name }}</h3>
                    <h4>{{ $product->price }} XOF</h4>
                    <p class="card-text">{{ $product->description }}</p>
                    {{-- <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span> --}}
                    {{-- 4.0 stars --}}
                    <div class="row mb-0 mt-2">
                        <button class="btn btn-buy mx-3">{{ __('Buy Now') }}</button>
                    </div>
                </div>
            </div>
            
            {{-- <div class="card card-outline-secondary my-4">
                <div class="card-header">
                    Product Reviews
                </div>
                <div class="card-body">
                    <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                    4.0
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
                    <small class="text-muted">Posted by Anonymous on 3/1/17</small>
                    <hr>
                    <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                    4.0
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
                    <small class="text-muted">Posted by Anonymous on 3/1/17</small>
                    <hr>
                    <span class="text-warning">&#9733; &#9733; &#9733; &#9733; &#9734;</span>
                    4.0
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis et enim aperiam inventore, similique necessitatibus neque non! Doloribus, modi sapiente laboriosam aperiam fugiat laborum. Sequi mollitia, necessitatibus quae sint natus.</p>
                    <small class="text-muted">Posted by Anonymous on 3/1/17</small>
                    <hr>
                </div>
            </div> --}}
        </div>
    </div>
</div>
@endsection
