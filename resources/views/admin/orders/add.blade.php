@extends('admin.layouts.app')

@section('content')
    
@section('content')
<h2>{{ __('Orders Management') }}</h2>

<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Form of products') }}
        </h6>
    </div>
    <div class="card-body">
        @isset($errors)
            @foreach($errors->all() as $error)
            <small class="text-danger">{{$error}}</small><br>
            @endforeach
        @endisset
        <form action="{{ route('deep.orders.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="user">Choose customer</label>
                        <input list="users" id="user" name="user" class="form-control  @error('user') is-invalid @enderror"  autocomplete="user"  value="{{ old('user') }}" required>
                        <datalist id="users">
                            @foreach ($users as $user)
                                <option value="{{$user->id.' - '.$user->name.' '.$user->surname}}">
                            @endforeach
                        </datalist>
                        @error('user')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                        {{-- <a href="{{ route('deep.users.add') }}" class="btn btn-link">Add a new customer</a> --}}
                        <a href="#" class="btn btn-link pl-0">Add a new customer</a>
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <label for="product">Choose product</label>
                        <input list="products" id="product" name="product" class="form-control  @error('product') is-invalid @enderror" autocomplete="product"  value="{{ old('product') }}" required>
                        <datalist id="products">
                            @foreach ($products as $product)
                                <option value="{{$product->id.' - '.$product->name}}">
                            @endforeach
                        </datalist>
                        @error('product')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                        <a href="{{ route('deep.products.add') }}" class="btn btn-link pl-0">Add a new product</a>
                    </div>
                </div>
            </div>
            <div class="form-group">
                
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="quantity">Quantity</label>
                        <input type="number" name="quantity" id="quantity" value="{{ old('quantity') }}" class="form-control  @error('quantity') is-invalid @enderror" autocomplete="is_shipped" required>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <label>State</label>
                        <div class="row">
                            <div class="col-sm-12 col-md-4">
                                <div class="form-check  @error('is_paid') is-invalid @enderror">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="is_paid" id="is_paid" value="@if(old('is_paid')) {{ old('is_paid') }} @else 1 @endif" checked>
                                    Paid ?
                                </label>
                                </div>
                                @error('is_paid')
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="form-check  @error('is_shipped') is-invalid @enderror">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="is_shipped" id="is_shipped" value="@if(old('is_shipped')) {{ old('is_shipped') }} @else 1 @endif" checked>
                                    Shipped ?
                                </label>
                                </div>
                                @error('is_shipped')
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="form-check  @error('is_complete') is-invalid @enderror">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="is_complete" id="is_complete" value="@if(old('is_complete')) {{ old('is_complete') }} @else 1 @endif" checked>
                                    Complete ?
                                </label>
                                </div>
                                @error('is_complete')
                                    <small class="text-danger">{{$message}}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group pl-0 col-sm-12 col-md-6">
                <label for="tracking_number">Tracking number</label>
                <input type="text" id="tracking_number" class="form-control @error('tracking_number') is-invalid @enderror" name="tracking_number" value="{{ old('tracking_number') }}" autocomplete="tracking_number">
                @error('tracking_number')
                    <small class="text-danger">{{$message}}</small>
                @enderror
            </div>
            <button class="btn btn-success" type="submit">{{ __('Submit') }}</button>
        </form>
    </div>
</div>
@endsection

@section('order-active')active @endsection