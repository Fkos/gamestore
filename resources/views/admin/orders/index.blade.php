@extends('admin.layouts.app')

@section('content')
<h2>{{ __('Orders Management') }}</h2>

<a href="{{ route('deep.orders.add') }}" class="btn btn-success"><i class="fa fa-fw fa-plus"></i> {{ __('Add') }}</a>

<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Table of orders') }}
        </h6>
    </div>
    <div class="card-body">
        @isset($orders)
            @if (count($orders) > 0)
                <table class="table table-bordered table-responsive-md">
                    <thead>
                        <tr>
                            <th>{{ __('Product') }}</th>
                            <th>{{ __('Customer') }}</th>
                            <th>{{ __('Quantity') }}</th>
                            <th>{{ __('Reference') }}</th>
                            <th>{{ __('Paid') }}</th>
                            <th>{{ __('Shipped') }}</th>
                            <th>{{ __('Completed') }}</th>
                            <th>{{ __('Cancelled') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __('Product') }}</th>
                            <th>{{ __('Customer') }}</th>
                            <th>{{ __('Quantity') }}</th>
                            <th>{{ __('Reference') }}</th>
                            <th>{{ __('Paid') }}</th>
                            <th>{{ __('Shipped') }}</th>
                            <th>{{ __('Completed') }}</th>
                            <th>{{ __('Cancelled') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($orders as $order)
                        <tr>
                            <td>{{ $order->product->name }}</td>
                            <td>{{ $order->user->name }} {{ $order->user->surname }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->reference }}</td>
                            <td>@if($order->is_paid) <i class="fas fa-check"></i> @else <div class="row py-auto px-2 justify-content-around"><i class="fas fa-times"></i> <form action="{{route('deep.orders.update',$order->id)}}" method="post">@csrf @method('put') <input type="checkbox" checked name="is_paid" hidden> <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-check"></i></button></form> </div>@endif</td>
                            <td>@if($order->is_shipped) <i class="fas fa-check"></i> @else <div class="row py-auto px-2 justify-content-around"><i class="fas fa-times"></i> <form action="{{route('deep.orders.update',$order->id)}}" method="post">@csrf @method('put') <input type="checkbox" checked name="is_shipped" hidden> <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-check"></i></button></form> </div>@endif</td>
                            <td>@if($order->is_complete) <i class="fas fa-check"></i> @else <div class="row py-auto px-2 justify-content-around"><i class="fas fa-times"></i> <form action="{{route('deep.orders.update',$order->id)}}" method="post">@csrf @method('put') <input type="checkbox" checked name="is_complete" hidden> <button type="submit" class="btn btn-success btn-sm"><i class="fas fa-check"></i></button></form> </div>@endif</td>
                            <td>@if($order->is_cancelled) <i class="fas fa-check"></i> @else <div class="row py-auto px-2 justify-content-around"><i class="fas fa-times"></i>@if(!$order->is_complete) <form action="{{route('deep.orders.update',$order->id)}}" method="post">@csrf @method('put') <input type="checkbox" checked name="is_cancelled" hidden> <button type="submit" class="btn btn-danger btn-sm"><i class="fas fa-check"></i></button></form> @endif</div>@endif</td>
                            <td>
                                <div class="row px-3 justify-content-arround">
                                    <a href="{{ route( 'deep.orders.show' , $order->id ) }}" class="btn btn-primary rounded mr-1 mb-1"><i class="fas fa-file"></i></a>
                                    <a href="{{ route( 'deep.orders.edit' , $order->id ) }}" class="btn btn-warning rounded mr-1 mb-1"><i class="fas fa-pen"></i></a>
                                    <button class="btn btn-danger rounded mr-1 mb-1"><i class="fas fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>    
                        @endforeach
                    </tbody>
                    
                </table>
            @else
            <div class="alert alert-info">
                <h3>{{ __('Info') }}</h3>
                <p>{{ __('No order found in the database') }}</p>
            </div>
            @endif
        @else
        <div class="alert alert-danger">
            <h3>{{ __('Error') }}</h3>
            <p>{{ __('We have been unable to fetch orders data') }}</p>
        </div>
        @endisset
    </div>
</div>
@endsection

@section('order-active')active @endsection