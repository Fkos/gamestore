@extends('admin.layouts.app')

@section('content')
<h2>{{ __('Categories Management') }}</h2>

<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Form of categoriess') }}
        </h6>
    </div>
    <div class="card-body">
        <form action="{{ route('deep.categories.store') }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" id="name" name="name" class="form-control  @error('name') is-invalid @enderror"  autocomplete="name"  value="{{ $category->name }}" required>
                        @error('name')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <label for="fa_class">{{ __('Font awesome class') }}</label>
                        <input type="text" id="fa_class" name="fa_class" class="form-control  @error('fa_class') is-invalid @enderror" autocomplete="fa_class"  value="{{ $category->fa_class }}">
                        @error('fa_class')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <button class="btn btn-success" type="submit">{{ __('Submit') }}</button>
        </form>
    </div>
</div>
@endsection

@section('category-active')active @endsection