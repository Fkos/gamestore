@extends('admin.layouts.app')

@section('content')
<h2>{{ __('Categories Management') }}</h2>

<a href="{{ route('deep.categories.add') }}" class="btn btn-success"><i class="fa fa-fw fa-plus"></i> {{ __('Add') }}</a>

<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Table of categories') }}
        </h6>
    </div>
    <div class="card-body">
        @isset($categories)
            @if (count($categories) > 0)
                <table class="table table-bordered table-responsive-md">
                    <thead>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Font Awesome') }}</th>
                            <th>{{ __('Products count') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Font Awesome') }}</th>
                            <th>{{ __('Products count') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->fa_class }} <i class="{{ $category->fa_class }}"></i></td>
                            <td>{{ count($category->products) }}</td>
                            <td>
                                <div class="row px-3 justify-content-arround">
                                    <a href="{{ route( 'deep.categories.show' , $category->id ) }}" class="btn btn-primary rounded mr-1 mb-1"><i class="fas fa-file"></i></a>
                                    <a href="{{ route( 'deep.categories.edit' , $category->id ) }}" class="btn btn-warning rounded mr-1 mb-1"><i class="fas fa-pen"></i></a>
                                    <button class="btn btn-danger rounded mr-1 mb-1"><i class="fas fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>    
                        @endforeach
                    </tbody>
                    
                </table>
            @else
            <div class="alert alert-info">
                <h3>{{ __('Info') }}</h3>
                <p>{{ __('No category found in the database') }}</p>
            </div>
            @endif
        @else
        <div class="alert alert-danger">
            <h3>{{ __('Error') }}</h3>
            <p>{{ __('We have been unable to fetch categories data') }}</p>
        </div>
        @endisset
    </div>
</div>
@endsection

@section('category-active')active @endsection