@extends('admin.layouts.app')

@section('content')
<h2>{{ __('Countries Management') }}</h2>

<a href="{{ route('deep.countries.add') }}" class="btn btn-success"><i class="fa fa-fw fa-plus"></i> {{ __('Add') }}</a>

<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Table of countries') }}
        </h6>
    </div>
    <div class="card-body">
        @isset($countries)
            @if (count($countries) > 0)
                <table class="table table-bordered table-responsive-md">
                    <thead>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Phone Prefix') }}</th>
                            <th>{{ __('Users count') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Phone Prefix') }}</th>
                            <th>{{ __('Users count') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($countries as $country)
                        <tr>
                            <td>{{ $country->name }}</td>
                            <td>{{ $country->phone_code }} <i class="{{ $country->phone_code }}"></i></td>
                            <td>{{ count($country->users) }}</td>
                            <td>
                                <div class="row px-3 justify-content-arround">
                                    <a href="{{ route( 'deep.countries.show' , $country->id ) }}" class="btn btn-primary rounded mr-1 mb-1"><i class="fas fa-file"></i></a>
                                    <a href="{{ route( 'deep.countries.edit' , $country->id ) }}" class="btn btn-warning rounded mr-1 mb-1"><i class="fas fa-pen"></i></a>
                                    <button class="btn btn-danger rounded mr-1 mb-1"><i class="fas fa-trash"></i></button>
                                </div>
                            </td>
                        </tr>    
                        @endforeach
                    </tbody>
                    
                </table>
            @else
            <div class="alert alert-info">
                <h3>{{ __('Info') }}</h3>
                <p>{{ __('No country found in the database') }}</p>
            </div>
            @endif
        @else
        <div class="alert alert-danger">
            <h3>{{ __('Error') }}</h3>
            <p>{{ __('We have been unable to fetch countries data') }}</p>
        </div>
        @endisset
    </div>
</div>
@endsection

@section('country-active')active @endsection