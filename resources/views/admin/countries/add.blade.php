@extends('admin.layouts.app')

@section('content')
<h2>{{ __('Countries Management') }}</h2>

<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Form of countries') }}
        </h6>
    </div>
    <div class="card-body">
        @isset($errors)
            @foreach($errors->all() as $error)
            <small class="text-danger">{{$error}}</small><br>
            @endforeach
        @endisset
        <form action="{{ route('deep.countries.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" id="name" name="name" class="form-control  @error('name') is-invalid @enderror"  autocomplete="name"  value="{{ old('name') }}" required autofocus>
                        @error('name')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>

                    <div class="col-sm-12 col-md-6">
                        <label for="phone_code">{{ __('Phone prefix') }}</label>
                        <input type="text" id="phone_code" name="phone_code" class="form-control  @error('phone_code') is-invalid @enderror" autocomplete="phone_code"  value="{{ old('phone_code') }}" required>
                        @error('phone_code')
                            <small class="text-danger">{{$message}}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <button class="btn btn-success" type="submit">{{ __('Submit') }}</button>
        </form>
    </div>
</div>
@endsection

@section('category-active')active @endsection