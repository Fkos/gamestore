@extends('admin.layouts.app')

@section('content')
<h2>{{ __('Products Management') }}</h2>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/deep">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Products</li>
    </ol>
</nav>

<a href="{{ route('deep.products.add') }}" class="btn btn-success"><i class="fa fa-fw fa-plus"></i> {{ __('Add') }}</a>

<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Table of products') }}
        </h6>
    </div>
    <div class="card-body">
        @isset($products)
            @if (count($products) > 0)
                <table class="table table-bordered table-responsive-md">
                    <thead>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Stock') }}</th>
                            <th>{{ __('Price') }}</th>
                            <th>{{ __('Shipping') }}</th>
                            <th>{{ __('Orders') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Stock') }}</th>
                            <th>{{ __('Price') }}</th>
                            <th>{{ __('Shipping') }}</th>
                            <th>{{ __('Orders') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->stock }}</td>
                            <td>{{ $product->price }}</td>
                            <td>{{ $product->shipping_price }}</td>
                            <td>{{ count($product->orders) }}</td>
                            <td>
                                <div class="row px-3 justify-content-arround">
                                    <a href="{{ route( 'deep.products.show' , $product->id ) }}" class="btn btn-primary rounded mr-1 mb-1"><i class="fas fa-file"></i></a>
                                    <a href="{{ route( 'deep.products.edit' , $product->id ) }}" class="btn btn-warning rounded mr-1 mb-1"><i class="fas fa-pen"></i></a>
                                    <form action="{{ route('deep.products.delete', $product->id) }}" method="POST" onsubmit="confirmdelete(event)">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger rounded mr-1 mb-1"><i class="fas fa-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>    
                        @endforeach
                    </tbody>
                    
                </table>
            @else
            <div class="alert alert-info">
                <h3>{{ __('Info') }}</h3>
                <p>{{ __('No product found in the database') }}</p>
            </div>
            @endif
        @else
        <div class="alert alert-danger">
            <h3>{{ __('Error') }}</h3>
            <p>{{ __('We have been unable to fetch products data') }}</p>
        </div>
        @endisset
    </div>
</div>
@endsection

@section('product-active')active @endsection

@push('end')
    <script>
        function confirmdelete(e){
            e.preventDefault();
            if(confirm("Do you really want to delete this product?")){
                e.target.submit();
            }
        }
    </script>
@endpush