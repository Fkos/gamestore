@extends('admin.layouts.app')

@section('content')
<h2>{{ __('Products Management') }}</h2>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/deep">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="/deep/products">Products</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ Str::substr($product->name, 0, 20) }}</li>
    </ol>
</nav>

<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $product->name }}</h3>
    </div>
    <div class="card-body">
        <div class="row">

            @if($product->video != null)
            <div class="col-sm-12 col-md-6">
                <figure class="figure border">
                    <figcaption class="figure-caption text-right">
                        <form action="{{ action('Deep\VideoController@delete',$product->video->id) }}" method="POST" onsubmit="confirmForm(event)">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger"><b>X</b></button>
                        </form>
                    </figcaption>
                    <video width="480" height="270" controls>
                        <source src="{{ asset($product->video->url) }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </figure>
            </div>
            @endif

            @if (count($product->images) > 0)

            @foreach ($product->images as $image)
            <div class="col-sm-12 col-md-3">
                <figure class="figure border">
                    <figcaption class="figure-caption text-right">
                        <form action="{{ action('Deep\ImageController@delete',$image->id) }}" method="POST" onsubmit="confirmform(event)">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger"><b>X</b></button>
                        </form>
                    </figcaption>
                    <img class="img-fluid" src="{{ asset($image->url) }}" alt="{{ $product->name }}">
                </figure>
            </div>
            @endforeach

            @endif
            </div>
        <button class="btn btn-link" data-toggle="modal" data-target="#add-video-modal">@if($product->video != null){{ __('Change the Video') }} @else {{ __('Add a Video') }}@endif</button>
        <button class="btn btn-link" data-toggle="modal" data-target="#add-images-modal">{{ __('Add more Images') }}</button>
        <h4>{{$product->price}} {{ __('FCFA') }}</h4>
        <p class="card-text">{{ $product->description }}</p>
    </div>
    <div class="card-footer">
        <div class="col-md-6 col-sm-12">
            <div class="row">
                <a href="{{ route( 'deep.products.edit' , $product->id ) }}" class="btn btn-warning m-1"><i class="fas fa-pencil-alt"></i></a>
                <form method="POST" action="{{ route( 'deep.products.delete' , $product->id ) }}">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger m-1"><i class="fas fa-trash"></i></button>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="add-images-modal" class="modal fade" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Add Images') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" action="{{ action('Deep\ImageController@store') }}" method="POST">
                    @csrf
                    <input type="number" name="product_id" value="{{$product->id}}" hidden>
                    <div class="form-group">
                        <div class="custom-file">
                            <label for="images" class="custom-file-label">{{ __('Choose your product images') }}</label>
                            <input type="file" accept="image/png,image/jpeg"  class="custom-file-input @error('images') is-invalid @enderror" name="images[]" id="images" placeholder="Images" multiple required>
                        </div>
                        <small id="images-help" class="form-text text-muted">{{ __('Only jpg and png format are accepted') }}</small>
                        @error('images')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="row mx-1 justify-content-end">
                        <button class="btn btn-success right-0" type="submit">{{ __('Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<div id="add-video-modal" class="modal fade" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">{{ __('Change video') }}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($product->video == null)
                <form enctype="multipart/form-data" action="{{ action('Deep\VideoController@store') }}" method="POST">
                @else
                <form enctype="multipart/form-data" action="{{ action('Deep\VideoController@update',$product->video->id) }}" method="POST">
                    @method('put')
                @endif
                    @csrf
                    <input type="number" name="product_id" value="{{$product->id}}" hidden>
                    <div class="form-group">
                        <label for="video">{{ __('Choose your product video') }}</label>
                        <div class="custom-file">
                            <input type="file" accept="video/mp4"  class="custom-file-input" name="video" id="video" placeholder="Video" aria-describedby="video-help" required>
                            <label for="video" class="custom-file-label @error('video') is-invalid @enderror">{{ __('Choose your product video') }}</label>
                        </div>
                        <small id="video-help" class="form-text text-muted">{{ __('Only mp4 format is accepted') }}</small>
                        @error('video')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="row mx-1 justify-content-end">
                        <button class="btn btn-success right-0" type="submit">{{ __('Submit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('product-active') active @endsection

@push("end")
<script>
    function confirmForm(e) {
        e.preventDefault();
        if(confirm('{{ __('Do you really want to delete this image?')}}')){
            e.currentTarget.submit();
        }
    }
</script>
@endpush
