@extends('admin.layouts.app')

@push('style')
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

/* Firefox */
input[type=number] {
    -moz-appearance: textfield;
}
@endpush

@section('product-active')active @endsection

@section('content')
<h2>{{ __('Products Management') }}</h2>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/deep">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="/deep/products">Products</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ __('Add') }}</li>
    </ol>
</nav>
<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Form of products') }}
        </h6>
    </div>
    <div class="card-body">
        <form action="{{ route('deep.products.update') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" id="name" class="form-control @error('name')is-invalid @enderror" name="name" value="{{ old('name') }}" autocomplete="name" autofocus required>
                        @error('name')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-3">
                        <label for="category">{{ __('Category') }}</label>
                        <select id="category" class="form-control @error('category')is-invalid @enderror" name="category" value="{{ old('category') }}" autocomplete="category" required>
                            <option value="" disabled selected>{{ __('Choose the product category') }}</option>
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">@if($category->fa_class != null)<i class="{{$category->fa_class}}"></i> @endif{{$category->name}}</option>
                            @endforeach
                        </select>
                        @error('category')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-3">
                        <label for="stock" title="{{ __('Leave empty if unlimited') }}">{{ __('Quantity') }}</label>
                        <input type="number" id="stock" class="form-control @error('stock')is-invalid @enderror" name="stock" value="{{ old('stock') }}" autocomplete="stock">
                        <small class="text-muted">{{ __('Leave empty if unlimited') }}</small>
                        @error('stock')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-3">
                        <label for="price">{{ __('Price') }}</label>
                        <input type="number" id="price" class="form-control @error('price')is-invalid @enderror" name="price" value="{{ old('price') }}" autocomplete="price" required>
                        @error('price')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <label for="shipping-price">{{ __('Shipping price') }}</label>
                        <input type="number" id="shipping-price" class="form-control @error('shipping_price')is-invalid @enderror" name="shipping_price" value="{{ old('shipping-price') }}" autocomplete="shipping-price">
                        @error('shipping_price')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <label for="email-shipping">Shipping method</label>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" name="can_ship_email" id="email-shipping" type="checkbox" value="1">{{ __('Is this product shipped by e-mail?') }}
                            </label>
                        </div>
                        @error('can_ship_email')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <label for="presentation">{{ __('Presentation') }}</label>
                    <input type="text" id="presentation" class="form-control @error('presentation')is-invalid @enderror" name="presentation" value="{{ old('presentation') }}" autocomplete="presentation" required>
                    <small class="text-muted">{{ __('A short summary of your product. it will be an introduction to the product') }}</small>
                    @error('presentation')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <label for="description">{{ __('Description') }}</label>
                    <textarea type="text" id="description" class="form-control @error('description')is-invalid @enderror" name="description" autocomplete="description" required>{{ old('description') }}</textarea>
                    @error('description')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>

            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">

                            <input type="file" accept="image/png,image/jpeg" name="images[]" id="images" placeholder="Choose Images" class="form-control form-control-file @error('images.*')is-invalid @enderror" aria-describedby="Choose images" multiple >

                        <small id="Choose images" class="form-text text-muted">{{ __('Choose jpg or png images to illustrate the product') }}</small>
                        @error('images.*')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-6">

                            <input type="file" accept="video/mp4" name="video" id="video" placeholder="Choose Video" class="form-control form-control-file @error('video')is-invalid @enderror" aria-describedby="Choose video">


                        <small id="Choose video" class="form-text text-muted">{{ __('Choose mp4 video to illustrate the product') }}</small>
                        @error('video')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>

            <button class="btn btn-success" type="submit">{{ __('Submit') }}</button>
        </form>
    </div>
</div>
@endsection
