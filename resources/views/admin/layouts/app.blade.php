<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>{{ config('app.name', 'Game Store - Admin ') }} @yield('title-complement')</title>
    
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/sb-admin.min.js') }}" defer></script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin.min.css') }}" rel="stylesheet">
    
    <style>
        /* @stack('style') */
    </style>
    @stack('head')
</head>
<body id="page-top">
    <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        
        <a class="navbar-brand mr-1" href="/deep/home">Game Store Admin</a>
        
        <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>
        
        
        <!-- Navbar -->
        <ul class="navbar-nav ml-auto ml-md-0">
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="fas fa-user-circle fa-fw"></i> {{ Auth::guard('deep')->user()->name }} <span class="caret"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                    
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        </ul>
        
    </nav>
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="sidebar navbar-nav">
            <li class="nav-item @yield('dashboard-active')">
                <a class="nav-link" href="{{ route('deep.home') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item  @yield('category-active') dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="categoryDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-list"></i>
                    <span>{{ __('Categories') }}</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="categoryDropdown">
                    <h6 class="dropdown-header">{{ __('Categories management') }}</h6>
                    <a class="dropdown-item" href="{{ route('deep.categories.add') }}">{{ __('Add') }}</a>
                    <a class="dropdown-item" href="{{ route('deep.categories') }}">{{ __('List') }}</a>
                </div>
            </li>
            <li class="nav-item  @yield('product-active') dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="productDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-box"></i>
                    <span>{{ __('Products') }}</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="productDropdown">
                    <h6 class="dropdown-header">{{ __('Products management') }}</h6>
                    <a class="dropdown-item" href="{{ route('deep.products.add') }}">{{ __('Add') }}</a>
                    <a class="dropdown-item" href="{{ route('deep.products') }}">{{ __('List') }}</a>
                    <a class="dropdown-item" href="{{ route('deep.products.summary') }}">{{ __('Summary') }}</a>
                </div>
            </li>
            <li class="nav-item  @yield('order-active') dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="OrderDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-shopping-cart"></i>
                    <span>{{ __('Orders') }}</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="OrderDropdown">
                    <h6 class="dropdown-header">{{ __('Orders management') }}</h6>
                    <a class="dropdown-item" href="{{ route('deep.orders') }}">{{ __('List') }}</a>
                    <a class="dropdown-item" href="#">{{ __('Summary') }}</a>
                </div>
            </li>
            <li class="nav-item  @yield('seller-active') dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="sellerDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-user-tie"></i>
                    <span>{{ __('Sellers') }}</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="sellerDropdown">
                    <h6 class="dropdown-header">{{ __('Sellers management') }}</h6>
                    <a class="dropdown-item" href="{{route('deep.sellers.add')}}">{{ __('Add') }}</a>
                    <a class="dropdown-item" href="{{route('deep.sellers')}}">{{ __('List') }}</a>
                </div>
            </li>
            <li class="nav-item  @yield('user-active') dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-users"></i>
                    <span>{{ __('Users') }}</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="userDropdown">
                    <h6 class="dropdown-header">{{ __('Users management') }}</h6>
                    <a class="dropdown-item" href="#">{{ __('Add') }}</a>
                    <a class="dropdown-item" href="#">{{ __('List') }}</a>
                </div>
            </li>
            <li class="nav-item  @yield('country-active') dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="countryDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-fw fa-flag"></i>
                    <span>{{ __('Countries') }}</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="countryDropdown">
                    <h6 class="dropdown-header">{{ __('Countries management') }}</h6>
                    <a class="dropdown-item" href="{{ route('deep.countries.add') }}">{{ __('Add') }}</a>
                    <a class="dropdown-item" href="{{ route('deep.countries') }}">{{ __('List') }}</a>
                </div>
            </li>
        </ul>
        
        
        <div id="content-wrapper">
            
            <div class="container-fluid">
                <main class="py-4 px-1">
                    <div class="container-fluid">
                        @if(isset($errors) && count($errors)>0)
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          @foreach ($errors->all() as $error)
                          @if($loop->index > 1)<br>@endif <strong>{{$error}}</strong>
                          @endforeach
                        </div>
                        
                        <script>
                          $(".alert").alert();
                        </script>
                        @endif
                        @yield('content')
                    </div>
                </main>
                <footer class="sticky-footer">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright © Your Game Store {{ (now()->year == '2020')? now()->year : '2020 - '.now()->year }}</span>
                        </div>
                    </div>
                </footer>
                
                <!-- Scroll to Top Button-->
                <a class="scroll-to-top rounded" href="#page-top">
                    <i class="fas fa-angle-up"></i>
                </a>
            </div>
        </div>
    </div>
    <script>
        window.onload = () => {
            $('table').DataTable();
        };
    </script>
    @stack('end')
</body>
</html>
