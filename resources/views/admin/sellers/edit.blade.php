@extends('admin.layouts.app')

@push('style')
/* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
}

/* Firefox */
input[type=number] {
    -moz-appearance: textfield;
}
@endpush

@section('seller-active')active @endsection

@section('content')
<h2>{{ __('Sellers Management') }}</h2>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/deep">Dashboard</a></li>
        <li class="breadcrumb-item"><a href="/deep/sellers">Sellers</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ __('Edit') }}</li>
    </ol>
</nav>
<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Form of sellers') }}
        </h6>
    </div>
    <div class="card-body">
        <form action="{{ route('deep.sellers.update', $seller->id) }}" method="POST">
            @csrf
            @method('put')
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-6">
                        <label for="name">{{ __('Name') }}</label>
                        <input type="text" id="name" class="form-control @error('name')is-invalid @enderror" name="name" value="{{ $seller->name }}" autocomplete="name" autofocus required>
                        @error('name')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="surname">{{ __('Surname') }}</label>
                        <input type="text" id="surname" class="form-control @error('surname')is-invalid @enderror" name="surname" value="{{ $seller->surname }}" autocomplete="surname" required>
                        @error('surname')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <div class="col-md-4">
                        <label for="email">{{ __('E-Mail') }}</label>
                        <input type="email" id="email" class="form-control @error('email')is-invalid @enderror" name="email" value="{{ $seller->email }}" autocomplete="email" required>
                        @error('email')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-4">
                        <label for="country">{{ __('Country') }}</label>
                        <select id="country" class="form-control @error('country')is-invalid @enderror" name="country" value="{{ $seller->country->id }}" autocomplete="country" required>
                            <option value="" disabled selected>{{ __('Choose the seller country') }}</option>
                            @foreach($countries as $country)
                            <option value="{{$country->id}}"@if($seller->country->id == $country->id) selected @endif>{{$country->name}} {{$country->phone_code}}</option>
                            @endforeach
                        </select>
                        @error('country')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-4">
                        <label for="phone" title="{{ __('Write it without country code') }}">{{ __('Phone') }}</label>
                        <input type="number" id="phone" class="form-control @error('phone')is-invalid @enderror" name="phone" value="{{ $seller->phone }}" autocomplete="phone" required>
                        @error('phone')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="form-row">
                    <label for="address">{{ __('Address') }}</label>
                    <input type="text" id="address" class="form-control @error('address')is-invalid @enderror" name="address" value="{{ $seller->address }}" autocomplete="address" required>
                    @error('address')
                        <small class="text-danger">{{ $message }}</small>
                    @enderror
                </div>
            </div>
            <div class="form-group">
                <label>Check the seller permissions</label>
                <div class="form-row">
                    <div class="col-md-4 col-lg-2">
                        <label>{{__('Sellers Management')}}</label>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_read_seller" id="can_read_seller" type="checkbox" @if($seller->can_read_seller) checked @endif  value="{{ $seller->can_read_seller }}">{{ __('Read sellers list') }}
                            </label>
                        </div>
                        @error('can_read_seller')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_create_seller" id="can_create_seller" type="checkbox" @if($seller->can_create_seller) checked @endif  value="{{ $seller->can_create_seller }}">{{ __('Create seller') }}
                            </label>
                        </div>
                        @error('can_create_seller')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_edit_seller" id="can_edit_seller" type="checkbox" @if($seller->can_update_seller) checked @endif  value="{{ $seller->can_update_seller }}">{{ __('Edit seller') }}
                            </label>
                        </div>
                        @error('can_edit_seller')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_delete_seller" id="can_delete_seller" type="checkbox" @if($seller->can_delete_seller) checked @endif  value="{{ $seller->can_delete_seller }}">{{ __('Delete seller') }}
                            </label>
                        </div>
                        @error('can_delete_seller')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-4 col-lg-2">
                        <label>{{__('Products Management')}}</label>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_read_product" id="can_read_product" type="checkbox" @if($seller->can_read_product) checked @endif  value="{{ $seller->can_read_product }}">{{ __('Read products list') }}
                            </label>
                        </div>
                        @error('can_read_product')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_create_product" id="can_create_product" type="checkbox" @if($seller->can_create_product) checked @endif  value="{{ $seller->can_create_product }}">{{ __('Create product') }}
                            </label>
                        </div>
                        @error('can_create_product')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_edit_product" id="can_edit_product" type="checkbox" @if($seller->can_update_product) checked @endif  value="{{ $seller->can_update_product }}">{{ __('Edit product') }}
                            </label>
                        </div>
                        @error('can_edit_product')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_delete_product" id="can_delete_product" type="checkbox" @if($seller->can_delete_product) checked @endif  value="{{ $seller->can_delete_product }}">{{ __('Delete product') }}
                            </label>
                        </div>
                        @error('can_delete_product')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-4 col-lg-2">
                        <label>{{__('Orders Management')}}</label>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_read_order" id="can_read_order" type="checkbox" @if($seller->can_read_order) checked @endif  value="{{ $seller->can_read_order }}">{{ __('Read orders list') }}
                            </label>
                        </div>
                        @error('can_read_order')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_create_order" id="can_create_order" type="checkbox" @if($seller->can_create_order) checked @endif  value="{{ $seller->can_create_order }}">{{ __('Create order') }}
                            </label>
                        </div>
                        @error('can_create_order')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_edit_order" id="can_edit_order" type="checkbox" @if($seller->can_update_order) checked @endif  value="{{ $seller->can_update_order }}">{{ __('Edit order') }}
                            </label>
                        </div>
                        @error('can_edit_order')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_delete_order" id="can_delete_order" type="checkbox" @if($seller->can_delete_order) checked @endif  value="{{ $seller->can_delete_order }}">{{ __('Delete order') }}
                            </label>
                        </div>
                        @error('can_delete_order')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-4 col-lg-2">
                        <label>{{__('Payements Management')}}</label>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_read_payement" id="can_read_payement" type="checkbox" @if($seller->can_read_payement) checked @endif  value="{{ $seller->can_read_payement }}">{{ __('Read payements list') }}
                            </label>
                        </div>
                        @error('can_read_payement')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_create_payement" id="can_create_payement" type="checkbox" @if($seller->can_create_payement) checked @endif  value="{{ $seller->can_create_payement }}">{{ __('Create payement') }}
                            </label>
                        </div>
                        @error('can_create_payement')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_edit_payement" id="can_edit_payement" type="checkbox" @if($seller->can_update_payement) checked @endif  value="{{ $seller->can_update_payement }}">{{ __('Edit payement') }}
                            </label>
                        </div>
                        @error('can_edit_payement')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_delete_payement" id="can_delete_payement" type="checkbox" @if($seller->can_delete_payement) checked @endif  value="{{ $seller->can_delete_payement }}">{{ __('Delete payement') }}
                            </label>
                        </div>
                        @error('can_delete_payement')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="col-md-4 col-lg-2">
                        <label>{{__('Customers Management')}}</label>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_read_customer" id="can_read_customer" type="checkbox" @if($seller->can_read_customer) checked @endif  value="{{ $seller->can_read_customer }}">{{ __('Read customers list') }}
                            </label>
                        </div>
                        @error('can_read_customer')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_create_customer" id="can_create_customer" type="checkbox" @if($seller->can_create_customer) checked @endif  value="{{ $seller->can_create_customer }}">{{ __('Create customer') }}
                            </label>
                        </div>
                        @error('can_create_customer')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_edit_customer" id="can_edit_customer" type="checkbox" @if($seller->can_update_customer) checked @endif  value="{{ $seller->can_update_customer }}">{{ __('Edit customer') }}
                            </label>
                        </div>
                        @error('can_edit_customer')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" value="1" name="can_delete_customer" id="can_delete_customer" type="checkbox" @if($seller->can_delete_customer) checked @endif  value="{{ $seller->can_delete_customer }}">{{ __('Delete customer') }}
                            </label>
                        </div>
                        @error('can_delete_customer')
                            <small class="text-danger">{{ $message }}</small>
                        @enderror
                    </div>
                </div>
            </div> 
            <button class="btn btn-success" type="submit">{{ __('Submit') }}</button>
        </form>
    </div>
</div>
@endsection