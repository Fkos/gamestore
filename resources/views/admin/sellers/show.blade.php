@extends('admin.layouts.app')

@section('content')
    <h2>{{ __('Sellers Management') }}</h2>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="/deep">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="/deep/sellers">Sellers</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $seller->name.' '.$seller->surname }}</li>
        </ol>
    </nav>
    <div class="card text-left">
        <div class="card-header">
            <h3>Seller Information</h3>
        </div>
      <div class="card-body">
        <h4 class="card-title">{{$seller->name.' '.$seller->surname}}</h4>
        <p class="card-text">
            <strong>E-mail : </strong> <span>{{$seller->email}}</span><br>
            <strong>Phone : </strong> <span>{{$seller->phone}}</span><br>
            <strong>Address : </strong> <span>{{$seller->address}}</span><br>
            <strong>Country : </strong> <span>{{$seller->country->name}}</span>
        </p>
        <div>
            <h4 class="card-title">Permissions</h4>
            <div class="row">
                <div class="col col-md-3 col-lg-2">
                    @if($seller->can_read_seller)
                    <span class="text-success">{{__('Can read sellers ')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot read sellers ')}}</span><br>
                    @endif
                    @if($seller->can_create_seller)
                    <span class="text-success">{{__('Can create seller')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot create seller')}}</span><br>
                    @endif
                    @if($seller->can_update_seller)
                    <span class="text-success">{{__('Can edit seller')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot edit seller')}}</span><br>
                    @endif
                    @if($seller->can_delete_seller)
                    <span class="text-success">{{__('Can delete seller')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot delete seller')}}</span><br>
                    @endif
                </div>
                <div class="col col-md-3 col-lg-2">
                    @if($seller->can_read_product)
                    <span class="text-success">{{__('Can read products ')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot read products ')}}</span><br>
                    @endif
                    @if($seller->can_create_product)
                    <span class="text-success">{{__('Can create product')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot create product')}}</span><br>
                    @endif
                    @if($seller->can_update_product)
                    <span class="text-success">{{__('Can edit product')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot edit product')}}</span><br>
                    @endif
                    @if($seller->can_delete_product)
                    <span class="text-success">{{__('Can delete product')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot delete product')}}</span><br>
                    @endif
                </div>
                <div class="col col-md-3 col-lg-2">
                    @if($seller->can_read_order)
                    <span class="text-success">{{__('Can read orders ')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot read orders ')}}</span><br>
                    @endif
                    @if($seller->can_create_order)
                    <span class="text-success">{{__('Can create order')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot create order')}}</span><br>
                    @endif
                    @if($seller->can_update_order)
                    <span class="text-success">{{__('Can edit order')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot edit order')}}</span><br>
                    @endif
                    @if($seller->can_delete_order)
                    <span class="text-success">{{__('Can delete order')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot delete order')}}</span><br>
                    @endif
                </div>
                <div class="col col-md-3 col-lg-2">
                    @if($seller->can_read_payement)
                    <span class="text-success">{{__('Can read payements ')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot read payements ')}}</span><br>
                    @endif
                    @if($seller->can_create_payement)
                    <span class="text-success">{{__('Can create payement')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot create payement')}}</span><br>
                    @endif
                    @if($seller->can_update_payement)
                    <span class="text-success">{{__('Can edit payement')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot edit payement')}}</span><br>
                    @endif
                    @if($seller->can_delete_payement)
                    <span class="text-success">{{__('Can delete payement')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot delete payement')}}</span><br>
                    @endif
                </div>
                <div class="col col-md-3 col-lg-2">
                    @if($seller->can_read_customer)
                    <span class="text-success">{{__('Can read customers ')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot read customers ')}}</span><br>
                    @endif
                    @if($seller->can_create_customer)
                    <span class="text-success">{{__('Can create customer')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot create customer')}}</span><br>
                    @endif
                    @if($seller->can_update_customer)
                    <span class="text-success">{{__('Can edit customer')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot edit customer')}}</span><br>
                    @endif
                    @if($seller->can_delete_customer)
                    <span class="text-success">{{__('Can delete customer')}}</span><br>
                    @else
                    <span class="text-muted">{{__('Cannot delete customer')}}</span><br>
                    @endif
                </div>
            </div>
        </div>
        <div class="row px-3 pt-2 justify-content-end">
            <a href="{{ route( 'deep.sellers.edit' , $seller->id ) }}" class="btn btn-warning rounded mr-1 mb-1"><i class="fas fa-pen"></i></a>
            <form action="{{ route('deep.sellers.delete', $seller->id) }}" method="POST" onsubmit="confirmdelete(event)">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger rounded mr-1 mb-1"><i class="fas fa-trash"></i></button>
            </form>
        </div>
      </div>
    </div>
@endsection

@push('end')
    <script>
        function confirmdelete(e){
            e.preventDefault();
            if(confirm("Do you really want to delete this seller's account?")){
                e.target.submit();
            }
        }
    </script>
@endpush