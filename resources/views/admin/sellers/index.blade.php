@extends('admin.layouts.app')

@section('content')
<h2>{{ __('Sellers Management') }}</h2>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/deep">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Sellers</li>
    </ol>
</nav>

<a href="{{ route('deep.sellers.add') }}" class="btn btn-success"><i class="fa fa-fw fa-plus"></i> {{ __('Add') }}</a>

<div class="card my-4">
    <div class="card-header">
        <h6>
            <i class="fas fa-fw fa-table"></i>
            {{ __('Table of sellers') }}
        </h6>
    </div>
    <div class="card-body">
        @isset($sellers)
            @if (count($sellers) > 0)
                <table class="table table-bordered table-responsive-md">
                    <thead>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Surname') }}</th>
                            <th>{{ __('E-Mail') }}</th>
                            <th>{{ __('Address') }}</th>
                            <th>{{ __('Phone') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Surname') }}</th>
                            <th>{{ __('E-Mail') }}</th>
                            <th>{{ __('Address') }}</th>
                            <th>{{ __('Phone') }}</th>
                            <th>{{ __('Actions') }}</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        @foreach ($sellers as $seller)
                        <tr>
                            <td>{{ $seller->name }}</td>
                            <td>{{ $seller->surname }}</td>
                            <td>{{ $seller->email }}</td>
                            <td>{{ $seller->address }}</td>
                            <td>{{ $seller->phone }}</td>
                            <td>
                                <div class="row px-3 justify-content-arround">
                                    <a href="{{ route( 'deep.sellers.show' , $seller->id ) }}" class="btn btn-primary rounded mr-1 mb-1"><i class="fas fa-file"></i></a>
                                    <a href="{{ route( 'deep.sellers.edit' , $seller->id ) }}" class="btn btn-warning rounded mr-1 mb-1"><i class="fas fa-pen"></i></a>
                                    <form action="{{ route('deep.sellers.delete', $seller->id) }}" method="POST" onsubmit="confirmdelete(event)">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger rounded mr-1 mb-1"><i class="fas fa-trash"></i></button>
                                    </form>
                                </div>
                            </td>
                        </tr>    
                        @endforeach
                    </tbody>
                    
                </table>
            @else
            <div class="alert alert-info">
                <h3>{{ __('Info') }}</h3>
                <p>{{ __('No seller found in the database') }}</p>
            </div>
            @endif
        @else
        <div class="alert alert-danger">
            <h3>{{ __('Error') }}</h3>
            <p>{{ __('We have been unable to fetch sellers data') }}</p>
        </div>
        @endisset
    </div>
</div>
@endsection

@section('seller-active')active @endsection

@push('end')
    <script>
        function confirmdelete(e){
            e.preventDefault();
            if(confirm("Do you really want to delete this seller's account?")){
                e.target.submit();
            }
        }
    </script>
@endpush