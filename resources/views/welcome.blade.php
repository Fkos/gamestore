@extends('layouts.app')

@push('style')
    .btn-buy{
      background-color : tomato;
      color : black;
    }

    .shipping_price {
      color : gray;
    }
@endpush

@section('content')
    <div class="container">

        <div class="row">

            <div class="col-lg-3 position-sticky">

                <h3 class="my-2">{{ __('Categories') }}</h3>
                <div class="list-group">
                    @foreach($categories as $category)
                        <a href="{{action('WelcomeController@productOfCategory', [$category->id, $category->name])}}" class="list-group-item text-dark"><i class="{{$category->fa_class}}"></i> {{ __($category->name) }}</a>
                    @endforeach
                </div>

            </div>
            <!-- /.col-lg-3 -->

            <div class="col-lg-9 my-2">
                {{-- <h5>{{ __('Sponsoring')}} </h5>
                <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                      <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="First slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
                    </div>
                    <div class="carousel-item">
                      <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Third slide">
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div> --}}
                {{--
                          <h3>{{ __('Trending')}} </h3>
                          <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
                            <ol class="carousel-indicators">
                              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner" role="listbox">
                              <div class="carousel-item active">
                                <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="First slide">
                              </div>
                              <div class="carousel-item">
                                <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Second slide">
                              </div>
                              <div class="carousel-item">
                                <img class="d-block img-fluid" src="http://placehold.it/900x350" alt="Third slide">
                              </div>
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                              <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                              <span class="carousel-control-next-icon" aria-hidden="true"></span>
                              <span class="sr-only">Next</span>
                            </a>
                          </div>
                           --}}
                <h3>{{ __('Recent Products')}}</h3>
                @isset($selected_category)
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/">{{__('Home')}}</a></li>
                            <li class="breadcrumb-item active" aria-current="page">{{ Str::substr($selected_category->name, 0, 20) }}</li>
                        </ol>
                    </nav>
                @endisset
                <div id="products-container" class="row">
                    @if(count($products) > 0)
                    @foreach ($products as $product)
                        {{-- @for ($i = 0; $i < 10; $i++)     --}}
                        <div class="col-lg-4 col-md-6 mb-4">
                            <div class="card h-100">
                              <a href="{{ action('ProductController@show', $product->id) }}"><img class="card-img-top" style="object-fit:contain;height:250px;width:100%" src="{{$product->images[0]->url}}" alt="{{ $product->name }}"></a>
                                <div class="card-body pb-0">
                                    <h4 class="card-title">
                                        <a href="{{ action('ProductController@show', $product->id) }}" class="text-dark">{{Str::substr($product->name,0,17)}}@if(Str::length($product->name)) ...@endif</a>
                                    </h4>
                                    <h5>{{$product->price}} FCFA</h5>
                                    <small class="shipping_price">{{($product->shipping_price != null && $product->shipping_price > 0)? '+ '.$product->shipping_price.' FCFA'.__(' Shipping Fees') :  __('Free Shipping')}}</small>
                                </div>
                                <div class="card-footer bg-white border-0 m-0">
                                    <div class="row justify-content-around"><a href="#" class="text-dark"><span class="fas fa-heart"></span></a><a href="#" class="text-dark"><span class="fas fa-shopping-cart"></span></a></div>
                                    <div class="row mb-0 mt-2">
                                      <button class="btn btn-buy btn-block">{{ __('Buy Now') }}</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        {{-- @endfor --}}
                    @endforeach
                    @else
                        <div class="alert alert-info ml-3">
                            <p>{{ __('Whoops! Nothing available here!') }}</p>
                        </div>
                    @endif

                </div>
                <!-- /.row -->

            </div>
            <!-- /.col-lg-9 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

    <!-- Footer -->
    <footer class="py-5 border-top shadow">
        <div class="container">
            <p class="m-0 text-center">Copyright &copy; Your Website 2020</p>
        </div>
        <!-- /.container -->
    </footer>

@endsection

{{-- @push('head')
    <script src="{{asset('js/pagination.min.js')}}" defer></script>
    <script>
      document.onload = () => {
        $('#products-container').pagination({
          dataSource: {!!$products!!},
          callback: function(data, pagination) {
            // template method of yourself
            var html = (data)=>{
              return `<div class="col-lg-4 col-md-6 mb-4">
                        <div class="card h-100 pb-0">
                          <a href="#"><img class="card-img-top" src="http://placehold.it/700x700" alt="${ data.name }"></a>
                          <div class="card-body">
                            <h4 class="card-title">
                              <a href="#" class="text-dark">${data.name}</a>
                            </h4>
                            <h5>${data.price} FCFA</h5>
                            <div class="row justify-content-around"><a href="#" class="text-dark"><span class="fas fa-heart"></span></a><a href="#" class="text-dark"><span class="fas fa-shopping-cart"></span></a></div>
                            <div class="row mb-0 mt-2">
                              <button class="btn btn-dark btn-block">{{ __('Buy Now') }}</button>
                            </div>
                          </div>
                        </div>
                      </div>`;
            };
            dataContainer.html(html);
          }
        })
      }
    </script>
@endpush --}}
