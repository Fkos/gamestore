<?php

namespace App;

use App\Model\Basket;
use App\Model\Country;
use App\Model\Favorite;
use App\Model\Order;
use App\Model\Product;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'surname', 'address', 'country_id', 'phone', 'want_newsletter'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
    
    public function baskets()
    {
        return $this->belongsToMany(Product::class,Basket::class,'product_id','user_id')->withPivot('quantity');
    }
    
    public function favorites()
    {
        return $this->belongsToMany(Product::class,Favorite::class,'product_id','user_id');
    }
}