<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        return Auth::guard('deep')->user()->can_edit_products;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'description' => 'required|string',
            'presentation' => 'required|string',
            'price' => 'required|numeric',
            'category' => 'required|numeric|exists:categories,id',
            'shipping_price' => 'nullable|numeric',
            'stock' => 'nullable|integer',
            'can_ship_email' => 'nullable|boolean',
        ];
    }
}
