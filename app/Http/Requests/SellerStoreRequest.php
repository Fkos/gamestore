<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SellerStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'surname' => 'required|string|max:50',
            'address' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|numeric',
            'country' => 'required|exists:countries,id',
            'can_read_order' => 'nullable|boolean',
            'can_create_order' => 'nullable|boolean',
            'can_edit_order' => 'nullable|boolean',
            'can_delete_order' => 'nullable|boolean',
            'can_read_customer' => 'nullable|boolean',
            'can_create_customer' => 'nullable|boolean',
            'can_edit_customer' => 'nullable|boolean',
            'can_delete_customer' => 'nullable|boolean',
            'can_read_product' => 'nullable|boolean',
            'can_create_product' => 'nullable|boolean',
            'can_edit_product' => 'nullable|boolean',
            'can_delete_product' => 'nullable|boolean',
            'can_read_seller' => 'nullable|boolean',
            'can_create_seller' => 'nullable|boolean',
            'can_edit_seller' => 'nullable|boolean',
            'can_delete_seller' => 'nullable|boolean',
            'can_read_payement' => 'nullable|boolean',
            'can_create_payement' => 'nullable|boolean',
            'can_edit_payement' => 'nullable|boolean',
            'can_delete_payement' => 'nullable|boolean',
        ];
    }
}
