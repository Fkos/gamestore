<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return Auth::guard('deep')->user()->can_create_orders;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->merge(['product_id' => explode(' - ',$this->product)[0],'user_id' => explode(' - ',$this->user)[0]]);
        // dd($this->all());
        return [
            'quantity' => 'required|numeric',
            'tracking_number' => 'nullable|string',
            'is_paid' => 'nullable',
            'is_shipped' => 'nullable',
            'is_complete' => 'nullable',
            'product' => 'required|regex:/^\d+ - /',
            'user' => 'required|regex:/^\d+ - /',
            'product_id' => 'exists:products,id',
            'user_id' => 'exists:users,id'
        ];
    }
}
