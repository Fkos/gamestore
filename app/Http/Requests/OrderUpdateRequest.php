<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return Auth::guard('deep')->user()->can_update_orders;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (isset($this->product)) {
            # code...
            $this->merge(['product_id' => explode(' - ',$this->product)[0]]);
        }
        if (isset($this->user)) {
            # code...
            $this->merge(['product_id' => explode(' - ',$this->product)[0]]);
        }
        return [
            'quantity' => 'nullable|numeric',
            'tracking_number' => 'nullable|string',
            'product' => 'nullable|regex:/^\d+ - /',
            'user' => 'nullable|regex:/^\d+ - /',
            'product_id' => 'nullable|exists:products,id',
            'user_id' => 'nullable|exists:users,id',
            'is_paid' => 'nullable',
            'is_shipped' => 'nullable',
            'is_complete' => 'nullable',
        ];
    }
}
