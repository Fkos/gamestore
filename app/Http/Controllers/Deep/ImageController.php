<?php

namespace App\Http\Controllers\Deep;

use App\Http\Controllers\Controller;
use App\Http\Requests\ImagesStoreRequest;
use App\Http\Requests\StoreImagesRequest;
use App\Model\Image;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    public function store(ImagesStoreRequest $request){
        $product = Product::find($request->product_id);

        foreach($request->images as $image){
            $path = $image->store('public/images');
            $image = new Image();
            $image->url = Storage::url($path);
            $image->product()->associate($product);
            $image->save();
        }
        return redirect(route('deep.products.show',$product->id));
    }

    public function delete($id){
        $image = Image::all()->find($id);
        $product = $image->product;
        if (count($product->images)>1){
            Storage::delete([str_replace('/storage/','', $image->url)]);
            $image->delete();
            return redirect(route('deep.products.show',$product->id));
        }else{
            return redirect()->back()->withErrors(['images'=>['Each product must have at least 1 image']]);
        }
    }
}
