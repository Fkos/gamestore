<?php

namespace App\Http\Controllers\Deep;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Model\Category;
use App\Model\Image;
use App\Model\Product;
use App\Model\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // if(Auth::guard('deep')->user()->can_read_products){
            $products = Product::all();
            return view('admin.products.index',['products' => $products]);
        // }else{
        //     abort(403);
        // }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.products.add',['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductStoreRequest $request)
    {
        $validated = $request->validated();

        $product = new Product();
        $product->name = $validated['name'];
        $product->price = $validated['price'];
        $product->shipping_price = $validated['shipping_price'];
        $product->presentation = $validated['presentation'];
        $product->description = $validated['description'];
        if( isset($validated['stock']) && $validated['stock']!=null){
            $product->stock = $validated['stock'];
            $product->can_stock = true;
        }
        if( isset($validated['can_ship_email'])){
            $product->can_ship_email = true;
        }
        if($validated['shipping_price'] == null){
            $product->shipping_price = 0;
        }else{
            $product->shipping_price = $validated['shipping_price'];
        }
        $category = Category::find($validated['category']);
        $product->category()->associate($category);
        $product->save();
        foreach($request->images as $image){
            $path = $image->store('public/images');
            $image = new Image();
            $image->url = Storage::url($path);
            $image->product()->associate($product);
            $image->save();
        }
        
        if($request->hasFile('video')){
            $path = $request->video->store('public/videos');
            $video = new Video();
            $video->url = Storage::url($path);
            $video->product()->associate($product);
            $video->save();
        }

        return redirect(route('deep.products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.products.show',['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all();
        return view('admin.products.edit',['product'=>$product, 'categories'=> $categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, $id)
    {
        $validated = $request->validated();

        $product = Product::find($id);
        $product->name = $validated['name'];
        $product->price = $validated['price'];
        $product->shipping_price = $validated['shipping_price'];
        $product->presentation = $validated['presentation'];
        $product->description = $validated['description'];
        if( isset($validated['stock']) && $validated['stock']!=null){
            $product->stock = $validated['stock'];
            $product->can_stock = true;
        }
        if( isset($validated['can_ship_email'])){
            $product->can_ship_email = true;
        }
        if($validated['shipping_price'] == null){
            $product->shipping_price = 0;
        }else{
            $product->shipping_price = $validated['shipping_price'];
        }
        $category = Category::find($validated['category']);
        $product->category()->associate($category);
        $product->save();

        return redirect(route('deep.products.show',$product->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        if($product != null){
            foreach($product->images as $image){
                Storage::disk('public')->delete([str_replace('/storage/','', $image->url)]);
            }
            $product->delete();
        }

        return redirect(route('deep.products'));
    }
}
