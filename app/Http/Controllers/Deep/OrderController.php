<?php

namespace App\Http\Controllers\Deep;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderStoreRequest;
use App\Http\Requests\OrderUpdateRequest;
use App\Model\Order;
use App\Model\Product;
use App\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::all();
        return view('admin.orders.index',['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::all(['id', 'name', 'surname']);
        $products = Product::all();
        return view('admin.orders.add', ['products' => $products, 'users' => $users]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderStoreRequest $request)
    {
        $validated = $request->validated();
        // dd($request->user_id);
        $order = new Order();
        $order->quantity = $validated['quantity'];
        $order->product_id = $request->product_id;
        $order->user_id = $request->user_id;
        $order->reference = 'P'.$request->product_id.'I'.crc32(uniqid());
        $order->is_paid = isset($validated['is_paid']);
        $order->is_shipped = isset($validated['is_shipped']);
        $order->is_complete = isset($validated['is_complete']);
        $order->tracking_number = $validated['tracking_number'];
        $order->is_cancelled = false;
        $order->save();
        return redirect(route('deep.orders'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderUpdateRequest $request, $id)
    {
        
        $validated = $request->all();
        // dd($request->user_id);
        $order = Order::all()->find($id);
        if (isset($validated['quantity'])) {
            $order->quantity = $validated['quantity'];
        }
        if (isset($request->product_id)) {
            # code...
            $order->product_id = $request->product_id;
        }
        if (isset($request->user_id)) {
            # code...
            $order->user_id = $request->user_id;
        }
        if (isset($request->is_paid)) {
            # code...
            $order->is_paid = isset($validated['is_paid']);
        }
        if (isset($request->is_shipped)) {
            # code...
            $order->is_shipped = isset($validated['is_shipped']);
        }
        if (isset($request->is_complete)) {
            # code...
            $order->is_complete = isset($validated['is_complete']);
        }
        if (isset($request->is_cancelled) && !$order->is_complete) {
            # code...
            $order->is_cancelled = true;
        }
        // dd('updating',$order);
        $order->save();
        return redirect(route('deep.orders'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
