<?php

namespace App\Http\Controllers\Deep;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreVideoRequest;
use App\Http\Requests\UpdateVideoRequest;
use App\Http\Requests\VideoStoreRequest;
use App\Http\Requests\VideoUpdateRequest;
use App\Model\Product;
use App\Model\Video;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function store(VideoStoreRequest $request){
        $product = Product::find($request->product_id);
        $video_file = $request->video;
        $path = $video_file->store('public/videos');
        $video = new Video();
        $video->url = Storage::url($path);
        $video->product()->associate($product);
        $video->save();
    
        return redirect(route('deep.products.show',$product->id));
    }

    public function update(VideoUpdateRequest $request){
        $product = Product::find($request->product_id);
        $video_file = $request->video;
        $video = Video::find($product->video->id);
        Storage::delete([$video->url]);
        $path = $video_file->store('public/videos');
        $video->url = Storage::url($path);
        $video->save();
    
        return redirect(route('deep.products.show',$product->id));
    }

    public function delete($id){
        $video = Video::all()->find($id);
        Storage::delete([$video->url]);
        $product = $video->product;
        $video->delete();
        return redirect(route('deep.products.show',$product->id));
    }
}
