<?php

namespace App\Http\Controllers\Deep;

use App\Http\Controllers\Controller;
use App\Http\Requests\SellerStoreRequest;
use App\Model\Country;
use App\Model\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sellers = Seller::all();
        return view('admin.sellers.index')->with('sellers',$sellers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('admin.sellers.add')->with('countries',$countries);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SellerStoreRequest $request)
    {
        // dd($request->all());
        $country = Country::find($request->country);
        $seller = new Seller();
        $seller->name = $request->name;
        $seller->surname =  $request->surname;
        $seller->address =  $request->address;
        $seller->email = $request->email;
        $seller->phone = $request->phone;
        $seller->password = Hash::make("password");
        if (isset($request->can_read_seller)) {
            $seller->can_read_seller = 1;
        }
        if (isset($request->can_create_seller)) {
            $seller->can_create_seller = 1;
        }
        if (isset($request->can_update_seller)) {
            $seller->can_update_seller = 1;
        }
        if (isset($request->can_delete_seller)) {
            $seller->can_delete_seller = 1;
        }
        if (isset($request->can_read_product)) {
            $seller->can_read_product = 1;
        }
        if (isset($request->can_create_product)) {
            $seller->can_create_product = 1;
        }
        if (isset($request->can_update_product)) {
            $seller->can_update_product = 1;
        }
        if (isset($request->can_delete_product)) {
            $seller->can_delete_product = 1;
        }
        if (isset($request->can_read_customer)) {
            $seller->can_read_customer = 1;
        }
        if (isset($request->can_create_customer)) {
            $seller->can_create_customer = 1;
        }
        if (isset($request->can_update_customer)) {
            $seller->can_update_customer = 1;
        }
        if (isset($request->can_delete_customer)) {
            $seller->can_delete_customer = 1;
        }
        if (isset($request->can_read_order)) {
            $seller->can_read_order = 1;
        }
        if (isset($request->can_create_order)) {
            $seller->can_create_order = 1;
        }
        if (isset($request->can_update_order)) {
            $seller->can_update_order = 1;
        }
        if (isset($request->can_delete_order)) {
            $seller->can_delete_order = 1;
        }
        if (isset($request->can_read_payement)) {
            $seller->can_read_payement = 1;
        }
        if (isset($request->can_create_payement)) {
            $seller->can_create_payement = 1;
        }
        if (isset($request->can_update_payement)) {
            $seller->can_update_payement = 1;
        }
        if (isset($request->can_delete_payement)) {
            $seller->can_delete_payement = 1;
        }
        $seller->country()->associate($country);
        $seller->save();
        return redirect()->route('deep.sellers');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $seller = Seller::find($id);
        return view('admin.sellers.show')->with('seller',$seller);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seller = Seller::find($id);
        
        if($seller != null){
            $countries = Country::all();
            return view('admin.sellers.edit')->with('seller',$seller)->with('countries',$countries);
        }
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $seller = Seller::find($id);
        if( $seller != null ){
            $country = Country::find($request->country);
            $seller->name = $request->name;
            $seller->surname =  $request->surname;
            $seller->address =  $request->address;
            $seller->email = $request->email;
            $seller->phone = $request->phone;
            if (isset($request->can_read_seller)) {
                $seller->can_read_seller = 1;
            }
            if (isset($request->can_create_seller)) {
                $seller->can_create_seller = 1;
            }
            if (isset($request->can_edit_seller)) {
                $seller->can_update_seller = 1;
            }
            if (isset($request->can_delete_seller)) {
                $seller->can_delete_seller = 1;
            }
            if (isset($request->can_read_product)) {
                $seller->can_read_product = 1;
            }
            if (isset($request->can_create_product)) {
                $seller->can_create_product = 1;
            }
            if (isset($request->can_edit_product)) {
                $seller->can_update_product = 1;
            }
            if (isset($request->can_delete_product)) {
                $seller->can_delete_product = 1;
            }
            if (isset($request->can_read_customer)) {
                $seller->can_read_customer = 1;
            }
            if (isset($request->can_create_customer)) {
                $seller->can_create_customer = 1;
            }
            if (isset($request->can_edit_customer)) {
                $seller->can_update_customer = 1;
            }
            if (isset($request->can_delete_customer)) {
                $seller->can_delete_customer = 1;
            }
            if (isset($request->can_read_order)) {
                $seller->can_read_order = 1;
            }
            if (isset($request->can_create_order)) {
                $seller->can_create_order = 1;
            }
            if (isset($request->can_edit_order)) {
                $seller->can_update_order = 1;
            }
            if (isset($request->can_delete_order)) {
                $seller->can_delete_order = 1;
            }
            if (isset($request->can_read_payement)) {
                $seller->can_read_payement = 1;
            }
            if (isset($request->can_create_payement)) {
                $seller->can_create_payement = 1;
            }
            if (isset($request->can_edit_payement)) {
                $seller->can_update_payement = 1;
            }
            if (isset($request->can_delete_payement)) {
                $seller->can_delete_payement = 1;
            }
            $seller->country()->associate($country);
            $seller->save();
            return redirect()->route('deep.sellers');
        }
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(count(Seller::all())>1){
            $seller = Seller::all()->find($id);
            if($seller != null){
                $seller->delete();
            }
            return redirect()->route('deep.sellers');
        }
        return redirect()->route('deep.sellers')->withErrors(['count'=>'There might be at least 1 seller in the system']);
    }
}
