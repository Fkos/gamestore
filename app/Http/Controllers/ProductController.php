<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\request;

class ProductController extends Controller {

    public function show($id){
        $categories = Category::all();
        $product = Product::find($id);
        if ($product != null){
            return view('details',  ['product' => $product, 'categories' => $categories]);
        }
        abort(404);
    }
}