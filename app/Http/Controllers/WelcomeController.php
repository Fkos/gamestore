<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        $products = Product::with(['images' => function ($query){
            $query->orderBy('created_at','asc');
        }])->get();

        return view('welcome',[
            'categories'=>$categories,
            'products'=>$products,
        ]);
    }

    public function productOfCategory($id)
    {
        $categories = Category::all();
        $category = $categories->find($id);
        if ($category != null) {
            $products = Product::with(['images' => function ($query) {
                $query->orderBy('created_at', 'asc');
            }])->where('category_id', $id)->get();

            return view('welcome', [
                'categories' => $categories,
                'selected_category' => $category,
                'products' => $products,
            ]);
        }
        abort(404);
    }
}
