<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function images(){
        return $this->hasMany(Image::class);
    }

    public function video(){
        return $this->hasOne(Video::class);
    }
    
    public function orders(){
        return $this->hasMany(Order::class);
    }
    
    public function category(){
        return $this->belongsTo(Category::class);
    }
}
