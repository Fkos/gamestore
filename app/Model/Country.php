<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public function users(){
        return $this->hasMany(User::class);
    }
    
    public function sellers(){
        return $this->hasMany(Seller::class);
    }
}
