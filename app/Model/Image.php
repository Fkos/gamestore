<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    function product(){
        return $this->belongsTo(Product::class);
    }
}
