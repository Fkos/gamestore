<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Payement extends Model
{
    public function order(){
        return $this->belongsTo(Order::class);
    }
}
