<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sellers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->string('phone');
            $table->string('address');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->boolean('can_read_seller')->default(false);
            $table->boolean('can_create_seller')->default(false);
            $table->boolean('can_update_seller')->default(false);
            $table->boolean('can_delete_seller')->default(false);
            $table->boolean('can_read_customer')->default(false);
            $table->boolean('can_create_customer')->default(false);
            $table->boolean('can_update_customer')->default(false);
            $table->boolean('can_delete_customer')->default(false);
            $table->boolean('can_read_order')->default(false);
            $table->boolean('can_create_order')->default(false);
            $table->boolean('can_update_order')->default(false);
            $table->boolean('can_delete_order')->default(false);
            $table->boolean('can_read_product')->default(false);
            $table->boolean('can_create_product')->default(false);
            $table->boolean('can_update_product')->default(false);
            $table->boolean('can_delete_product')->default(false);
            $table->boolean('can_read_payement')->default(false);
            $table->boolean('can_create_payement')->default(false);
            $table->boolean('can_update_payement')->default(false);
            $table->boolean('can_delete_payement')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sellers');
    }
}
