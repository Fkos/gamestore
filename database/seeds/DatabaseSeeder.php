<?php

use App\Model\Country;
use App\Model\Seller;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $country = new Country();
        $country->name = "Togo";
        $country->phone_code = 228;
        $country->save();

        $seller = new Seller();
        $seller->name = "Kotch";
        $seller->surname = "Fan";
        $seller->address = "Agoe-cacavelli";
        $seller->email = "kosson.fk@gmail.com";
        $seller->phone = 90381166;
        $seller->password = Hash::make("password");
        $seller->can_read_seller = 1;
        $seller->can_create_seller = 1;
        $seller->can_update_seller = 1;
        $seller->can_delete_seller = 1;
        $seller->can_read_product = 1;
        $seller->can_create_product = 1;
        $seller->can_update_product = 1;
        $seller->can_delete_product = 1;
        $seller->can_read_customer = 1;
        $seller->can_create_customer = 1;
        $seller->can_update_customer = 1;
        $seller->can_delete_customer = 1;
        $seller->can_read_order = 1;
        $seller->can_create_order = 1;
        $seller->can_update_order = 1;
        $seller->can_delete_order = 1;
        $seller->can_read_payement = 1;
        $seller->can_create_payement = 1;
        $seller->can_update_payement = 1;
        $seller->can_delete_payement = 1;
        $seller->country()->associate($country);
        $seller->save();

    }
}
