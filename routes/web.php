<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('setlocale/{locale}',function($lang){
    if (! in_array($lang, ['en', 'fr'])) {
        abort(400);
    }
    Session::put('locale',$lang);
    return redirect()->back();
});

Route::group(['middleware' => ['language']], function () {
    Auth::routes();
    Route::get('/', 'WelcomeController@index');
    Route::get('/categories/{id}-{name}/products', 'WelcomeController@productOfCategory');
    Route::get('products/{id}', 'ProductController@show');
    Route::get('/home', 'HomeController@index')->name('home');

    Route::prefix('deep')->name('deep.')->namespace('Deep')->group(function () {
        Auth::routes(['register' => false,'reset' => false, 'verify' => false]);

        Route::middleware('auth:deep')->group( function () {

            Route::get('/home', 'HomeController@index')->name('home');

            //Products routes
            Route::get('/products', 'ProductController@index')->name('products');
            Route::get('/products/summary', 'ProductController@summary')->name('products.summary');
            Route::get('/products/add', 'ProductController@create')->name('products.add');
            Route::get('/products/{id}', 'ProductController@show')->name('products.show');
            Route::get('/products/{id}/edit', 'ProductController@edit')->name('products.edit');
            Route::post('/products', 'ProductController@store')->name('products.store');
            Route::put('/products/{id}', 'ProductController@update')->name('products.update');
            Route::delete('/products/{id}', 'ProductController@destroy')->name('products.delete');

            //Orders routes
            Route::get('/orders', 'OrderController@index')->name('orders');
            Route::get('/orders/add', 'OrderController@create')->name('orders.add');
            Route::get('/orders/{id}', 'OrderController@show')->name('orders.show');
            Route::get('/orders/{id}/edit', 'OrderController@edit')->name('orders.edit');
            Route::post('/orders', 'OrderController@store')->name('orders.store');
            Route::put('/orders/{id}', 'OrderController@update')->name('orders.update');

            //Categories routes
            Route::get('/categories', 'CategoryController@index')->name('categories');
            Route::get('/categories/add', 'CategoryController@create')->name('categories.add');
            Route::get('/categories/{id}', 'CategoryController@show')->name('categories.show');
            Route::get('/categories/{id}/edit', 'CategoryController@edit')->name('categories.edit');
            Route::post('/categories', 'CategoryController@store')->name('categories.store');
            Route::put('/categories/{id}', 'CategoryController@update')->name('categories.update');

            //Countries routes
            Route::get('/countries', 'CountryController@index')->name('countries');
            Route::get('/countries/add', 'CountryController@create')->name('countries.add');
            Route::get('/countries/{id}', 'CountryController@show')->name('countries.show');
            Route::get('/countries/{id}/edit', 'CountryController@edit')->name('countries.edit');
            Route::post('/countries', 'CountryController@store')->name('countries.store');
            Route::put('/countries/{id}', 'CountryController@update')->name('countries.update');

            //Sellers routes
            Route::get('/sellers', 'SellerController@index')->name('sellers');
            Route::get('/sellers/add', 'SellerController@create')->name('sellers.add');
            Route::get('/sellers/{id}', 'SellerController@show')->name('sellers.show');
            Route::get('/sellers/{id}/edit', 'SellerController@edit')->name('sellers.edit');
            Route::post('/sellers', 'SellerController@store')->name('sellers.store');
            Route::put('/sellers/{id}', 'SellerController@update')->name('sellers.update');
            Route::delete('/sellers/{id}', 'SellerController@destroy')->name('sellers.delete');

            //Images routes
            Route::post('/images', 'ImageController@store')->name('images.store');
            Route::delete('/images/{id}', 'ImageController@delete')->name('images.delete');

            //Videos routes
            Route::post('/videos', 'VideoController@store')->name('videos.store');
            Route::delete('/videos/{id}', 'VideoController@delete')->name('videos.delete');
            Route::put('/videos/{id}', 'VideoController@update')->name('videos.update');
        });
    });
});
